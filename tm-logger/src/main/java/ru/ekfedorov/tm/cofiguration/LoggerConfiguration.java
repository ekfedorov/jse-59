package ru.ekfedorov.tm.cofiguration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.ekfedorov.tm")
public class LoggerConfiguration {
}
