package ru.ekfedorov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.model.TaskGraph;

import java.util.List;

public interface IProjectTaskGraphService {

    void bindTaskByProject(
            @Nullable String userId, @Nullable String projectId, @Nullable String taskId
    );

    @NotNull
    List<TaskGraph> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String taskId);

}
