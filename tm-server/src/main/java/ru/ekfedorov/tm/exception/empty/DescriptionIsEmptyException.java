package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class DescriptionIsEmptyException extends AbstractException {

    public DescriptionIsEmptyException() {
        super("Error! Description is empty...");
    }

}
