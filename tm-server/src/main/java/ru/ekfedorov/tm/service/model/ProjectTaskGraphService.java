package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ekfedorov.tm.api.repository.model.IProjectGraphRepository;
import ru.ekfedorov.tm.api.repository.model.ITaskGraphRepository;
import ru.ekfedorov.tm.api.service.model.IProjectTaskGraphService;
import ru.ekfedorov.tm.exception.empty.ProjectIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.TaskIdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.model.TaskGraph;

import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Service
public final class ProjectTaskGraphService implements IProjectTaskGraphService {

    @NotNull
    @Autowired
    public ITaskGraphRepository taskGraphRepository;

    @NotNull
    @Autowired
    public IProjectGraphRepository projectGraphRepository;

    @NotNull
    public ITaskGraphRepository getTaskRepository() {
        return taskGraphRepository;
    }

    @NotNull
    public IProjectGraphRepository getProjectRepository() {
        return projectGraphRepository;
    }

    @SneakyThrows
    @Override
    public void bindTaskByProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        taskRepository.bindTaskByProjectId(userId, projectId, taskId);
    }

    @NotNull
    @SneakyThrows
    @Override
    public List<TaskGraph> findAllByProjectId(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        return taskRepository.findAllByProjectId(userId, projectId);
    }

    @SneakyThrows
    @Override
    public void removeProjectById(
            @Nullable final String userId, @Nullable final String projectId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(projectId)) throw new ProjectIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        @NotNull final IProjectGraphRepository projectRepository = getProjectRepository();
        taskRepository.removeAllByProjectId(userId, projectId);
        projectRepository.removeOneByIdAndUserId(userId, projectId);
    }

    @SneakyThrows
    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId, @Nullable final String taskId
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(taskId)) throw new TaskIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getTaskRepository();
        taskRepository.unbindTaskFromProjectId(userId, taskId);
    }

}
