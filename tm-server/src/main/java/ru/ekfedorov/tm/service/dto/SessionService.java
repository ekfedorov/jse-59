package ru.ekfedorov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.api.repository.dto.ISessionRepository;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.api.service.dto.ISessionService;
import ru.ekfedorov.tm.api.service.dto.IUserService;
import ru.ekfedorov.tm.dto.Session;
import ru.ekfedorov.tm.dto.User;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.LoginIsEmptyException;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.UserIsLockedException;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Service
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    public ISessionRepository sessionRepository;

    @NotNull
    public ISessionRepository getRepository() {
        return sessionRepository;
    }

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final Session session) {
        if (session == null) throw new NullObjectException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.add(session);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Session> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        entities.forEach(sessionRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean contains(@NotNull final String id) {
        @NotNull final ISessionRepository sessionRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return sessionRepository.findOneById(id).isPresent();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Session> findAll() {
        @NotNull final ISessionRepository sessionRepository = getRepository();
        return sessionRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Session> findAllSort(
            @Nullable final String sort
    ) {
        @NotNull final ISessionRepository sessionRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<Session> comparator = sortType.getComparator();
        return sessionRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Optional<Session> findOneById(
            @Nullable final String id
    ) {
        @NotNull final ISessionRepository sessionRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return sessionRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.removeOneById(id);
    }

    @SneakyThrows
    @Transactional
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (isEmpty(login) || isEmpty(password)) return false;
        final @NotNull Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) return false;
        if (user.get().isLocked()) throw new UserIsLockedException();
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.get().getPasswordHash());
    }

    @Override
    @Nullable
    @SneakyThrows
    @Transactional
    public Session close(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.removeOneById(session.getId());
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @NotNull Optional<User> user = userService.findByLogin(login);
        if (!user.isPresent()) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.get().getId());
        @Nullable final Session signSession = sign(session);
        if (signSession == null) return null;
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.add(signSession);
        return signSession;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Session entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final ISessionRepository sessionRepository = getRepository();
        sessionRepository.removeOneById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        @NotNull final ISessionRepository sessionRepository = getRepository();
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.findOneById(session.getId()).isPresent()) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void validateAdmin(@Nullable final Session session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        validate(session);
        final @NotNull Optional<User> user = userService.findOneById(session.getUserId());
        if (!user.isPresent()) throw new AccessDeniedException();
        if (user.get().getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Nullable
    @Transactional
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
