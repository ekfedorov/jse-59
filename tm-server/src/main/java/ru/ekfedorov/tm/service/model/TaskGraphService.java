package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.api.repository.model.ITaskGraphRepository;
import ru.ekfedorov.tm.api.repository.model.IUserGraphRepository;
import ru.ekfedorov.tm.api.service.model.ITaskGraphService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.TaskGraph;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;
import static ru.ekfedorov.tm.util.ValidateUtil.notNullOrLessZero;

@Service
public final class TaskGraphService extends AbstractGraphService<TaskGraph> implements ITaskGraphService {

    @NotNull
    @Autowired
    public ITaskGraphRepository taskGraphRepository;

    @NotNull
    @Autowired
    public IUserGraphRepository userGraphRepository;

    @NotNull
    public ITaskGraphRepository getRepository() {
        return taskGraphRepository;
    }

    @NotNull
    public IUserGraphRepository getUserRepository() {
        return userGraphRepository;
    }

    @SneakyThrows
    @Override
    public @NotNull TaskGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        @NotNull final TaskGraph task = new TaskGraph();
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        task.setUser(userRepository.findOneById(userId).get());
        taskRepository.add(task);
        return task;
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
            taskRepository.clearByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public List<TaskGraph> findAll(@Nullable final String userId) {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        return taskRepository.findAllByUserId(userId);
    }

    @SneakyThrows
    @NotNull
    @Override
    public Optional<TaskGraph> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<TaskGraph> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<TaskGraph> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final TaskGraph entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
            taskRepository.removeOneById(entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final TaskGraph entity
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (entity == null) throw new NullObjectException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        @NotNull Optional<TaskGraph> task = taskRepository.findOneByIndex(userId, index);
        if (!task.isPresent()) throw new UserNotFoundException();
        taskRepository.removeOneByIdAndUserId(userId, task.get().getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final TaskGraph task) {
        if (task == null) throw new NullObjectException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
            taskRepository.add(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<TaskGraph> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        entities.forEach(taskRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
            taskRepository.clear();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskRepository.findOneById(id).isPresent();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskGraph> findAll() {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskGraph> findAllSort(
            @Nullable final String sort
    ) {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<TaskGraph> comparator = sortType.getComparator();
            return taskRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<TaskGraph> findOneById(
            @Nullable final String id
    ) {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    public @NotNull List<TaskGraph> findAll(@Nullable String userId, @Nullable String sort) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<TaskGraph> comparator = sortType.getComparator();
            return taskRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return;
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

}
