package ru.ekfedorov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.api.repository.dto.IProjectRepository;
import ru.ekfedorov.tm.api.service.dto.IProjectService;
import ru.ekfedorov.tm.dto.Project;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    public IProjectRepository projectRepository;

    @NotNull
    public IProjectRepository getRepository() {
        return projectRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.add(entity);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<Project> entities) {
        if (entities == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        entities.forEach(projectRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean contains(@NotNull final String id) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return projectRepository.findOneById(id).isPresent();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Project> findAll() {
        @NotNull final IProjectRepository projectRepository = getRepository();
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Project> findAllSort(
            @Nullable final String sort
    ) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<Project> comparator = sortType.getComparator();
        return projectRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Optional<Project> findOneById(
            @Nullable final String id
    ) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        return projectRepository.findOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public Project add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.add(project);
        return project;
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.clearByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public List<Project> findAll(@Nullable final String userId) {
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        return projectRepository.findAllByUserId(userId);
    }


    @SneakyThrows
    @NotNull
    @Override
    @Transactional
    public Optional<Project> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return projectRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public Optional<Project> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public Optional<Project> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(name)) throw new NameIsEmptyException();
         return projectRepository.findOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final Project entity) {
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneById(entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final Project entity
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (entity == null) throw new NullObjectException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneByIdAndUserId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneByIdAndUserId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        @NotNull Optional<Project> project = projectRepository.findOneByIndex(userId, index);
        if (!project.isPresent()) throw new UserNotFoundException();projectRepository.removeOneByIdAndUserId(userId, project.get().getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.removeOneByName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<Project> findAll(@Nullable String userId, @Nullable String sort) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        @NotNull final IProjectRepository projectRepository = getRepository();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
        @NotNull final Comparator<Project> comparator = sortType.getComparator();
        return projectRepository
                .findAllByUserId(userId)
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<Project> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return;
        @NotNull final Optional<Project> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<Project> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

}
